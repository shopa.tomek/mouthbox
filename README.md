<h1><strong>Mouthbox<strong></strong></h1>
<P>Mouthbox is a simple app designed to create
    short repeatable sounds, beat sequences by recording your own sound samples 
    in the browser.
</P>

<h2>Dependencies</h2>
Vue js 3

<h1>Contribution</h1>

<p>App is based on two awesome minimalistic components combined together:
    <strong>https://github.com/CaastOS/midiplayer</strong> and <strong>https://github.com/codeplayer71/jamit-audio-recorder</strong>
</p>

<h2>Future</h2>

<p>Project is still developing ! It will be some more functions added!</p>
